FactoryBot.define do
  factory :schedule, class: Schedule do
    time { (Time.now + rand(-24..24).hours).strftime('%H:%m') }
    day { Date::DAYNAMES.sample.downcase }
    duration { rand(60..180) }
    classroom { association :classroom }
  end
end
