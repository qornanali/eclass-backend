FactoryBot.define do
  factory :study_session, class: StudySession do
    schedule { association :schedule }
  end
end
