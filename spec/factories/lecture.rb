FactoryBot.define do
  factory :lecture, class: Lecture do
    student { association :subject }
    classroom { association :classroom }
  end
end
