FactoryBot.define do
  factory :attendance, class: Attendance do
    web_user { association :web_user }
    study_session { association :study_session }
  end
end
