FactoryBot.define do
  factory :web_user, class: WebUser do
    email { Faker::Internet.email }
    password { Faker::Lorem.characters(number: 20) }
  end
end
