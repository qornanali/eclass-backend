FactoryBot.define do
  factory :teacher, class: Teacher do
    full_name { Faker::Name.name }
    bio { Faker::Lorem.sentence }
    gender { Teacher::ALLOWED_GENDERS.sample }
    phone { Faker::PhoneNumber.phone_number }
    account { association :web_user }
  end
end
