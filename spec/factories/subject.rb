FactoryBot.define do
  factory :subject, class: Subject do
    name { Faker::Book.genre }
  end
end
