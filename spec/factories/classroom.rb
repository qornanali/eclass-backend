FactoryBot.define do
  factory :classroom, class: Classroom do
    name { Faker::Name.name }
    description { Faker::Lorem.sentence }
  end
end
