FactoryBot.define do
  factory :cms_user, class: CmsUser do
    name { Faker::Name.name }
    client_id { Faker::Lorem.word }
    password { Faker::Lorem.characters(number: 20) }
  end
end
