FactoryBot.define do
  factory :employment, class: Employment do
    teacher { association :teacher }
    classroom { association :classroom }
  end
end
