FactoryBot.define do
  factory :enrollment, class: Enrollment do
    student { association :student }
    classroom { association :classroom }
  end
end
