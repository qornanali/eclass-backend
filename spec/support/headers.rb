require 'rails_helper'

module Header
  def admin_headers
    {
      'Authorization' => create_basic_header(
        id: Figaro.env.ADMIN_ID,
        secret: Figaro.env.ADMIN_SECRET
      )
    }
  end

  def cms_headers
    password = Faker::Lorem.characters(number: 30)
    cms_user = create :cms_user, password: password
    {
      'Authorization' => create_basic_header(
        id: cms_user.client_id,
        secret: password
      )
    }
  end

  def web_headers
    token = Faker::Lorem.characters(number: 30)
    {
      'Authorization' => "Bearer #{token}"
    }
  end

  def create_basic_header(id:, secret:)
    ActionController::HttpAuthentication::Basic.encode_credentials(id, secret)
  end
end

RSpec.configure do |config|
  config.include Header, type: :request
end
