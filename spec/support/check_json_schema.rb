RSpec.shared_examples 'check json schema' do |schema_path, model, extra_attributes, exclude_superclass|
  context "Given #{model}" do
    let!(:model_object) { create(model, extra_attributes) }

    it 'should return json that matches schema', :shared do
      result = described_class.new(model_object).attributes
      result.except!(*described_class.superclass.new(model_object).attributes.keys) if exclude_superclass
      expect(result).to match_response_schema(schema_path)
    end
  end
end
