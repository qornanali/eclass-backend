require 'rails_helper'

RSpec.describe Response do
  describe '.succeed' do
    subject { described_class.succeed(params) }

    let(:params) do
      {
        message: message,
        data: data
      }
    end
    let(:message) { Faker::Lorem.sentence }
    let(:data) do
      [
        { Faker::Lorem.word => Faker::Lorem.word },
        [{ Faker::Lorem.word => Faker::Lorem.word }]
      ].sample
    end
    let(:response) { instance_double Response }

    before do
      allow(Response).to receive(:new).with(data: data, message: message).and_return(response)
    end

    it { is_expected.to eq response }

    context 'without message' do
      subject { described_class.succeed(params.except(:message)) }

      let(:message) { Response::SUCCEED_DEFAULT_MESSAGE }

      it { is_expected.to eq response }
    end
  end

  describe '.failed' do
    subject { described_class.failed(params) }

    let(:params) do
      {
        message: message
      }
    end
    let(:message) { Faker::Lorem.sentence }
    let(:response) { instance_double Response }

    before do
      allow(Response).to receive(:new).with(message: message).and_return(response)
    end

    it { is_expected.to eq response }

    context 'without message' do
      subject { described_class.failed(params.except(:message)) }

      let(:message) { Response::FAILED_DEFAULT_MESSAGE }

      it { is_expected.to eq response }
    end
  end
end
