require 'rails_helper'

RSpec.describe WebUserSession, type: :model do
  it { should validate_presence_of(:auth_token) }
  it { should validate_presence_of(:expired_at) }
end
