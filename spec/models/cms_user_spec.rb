require 'rails_helper'

RSpec.describe CmsUser, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:client_id) }
  it { should validate_presence_of(:password) }
  it { should validate_uniqueness_of(:client_id) }
  it { should validate_length_of(:password).is_at_most(72) }
end
