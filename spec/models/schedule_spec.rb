require 'rails_helper'

RSpec.describe Schedule, type: :model do
  let(:allowed_days) { Date::DAYNAMES.map(&:downcase) }

  it { should validate_uniqueness_of(:guid) }
  it { should allow_values(nil).for(:guid) }
  it { should validate_presence_of(:duration) }
  it { should validate_numericality_of(:duration).only_integer }
  it { should validate_presence_of(:time) }
  it { should validate_presence_of(:day) }
  it { should validate_inclusion_of(:day).in_array(allowed_days) }
  it { should_not allow_values(Faker::Lorem.characters(number: (1..5))).for(:time) }
  it { should allow_values((Time.now + rand(-24..24).hours).strftime('%H:%m')).for(:time) }

  it { should have_many(:study_sessions) }
  it { should belong_to(:classroom) }

  describe '#time_should_have_correct_value' do
    subject { model.valid? }
    let(:model) { build :schedule, time: time }

    let(:hour) { rand(0..23) }
    let(:minute) { rand(0..59) }
    let(:time) { "#{hour}:#{minute}" }

    it { is_expected.to eq true }

    context 'when hour is higher than 23' do
      let(:hour) { rand(24..30) }

      it { is_expected.to eq false }
    end

    context 'when hour is lower than 0' do
      let(:hour) { rand(-20..-1) }

      it { is_expected.to eq false }
    end

    context 'when minute is higher than 59' do
      let(:minute) { rand(60..70) }

      it { is_expected.to eq false }
    end

    context 'when minute is lower than 0' do
      let(:minute) { rand(-20..-1) }

      it { is_expected.to eq false }
    end
  end

  describe 'when create success' do
    describe '#guid' do
      subject { model.guid }

      let(:model) { create :schedule }
      let(:generated_guid) do
        "#{described_class.name.upcase}-#{model.id}-#{model.created_at.to_i.to_s(36)}"
      end

      it { is_expected.to eq generated_guid }
    end
  end
end
