require 'rails_helper'

RSpec.describe Student, type: :model do
  let(:allowed_genders) { %w[male female] }

  it { should validate_presence_of(:full_name) }
  it { should validate_uniqueness_of(:guid) }
  it { should allow_values(nil).for(:guid) }
  it { should validate_length_of(:bio).is_at_most(100) }
  it { should validate_length_of(:full_name).is_at_most(71) }
  it { should validate_inclusion_of(:gender).in_array(allowed_genders) }
  it { should validate_presence_of(:phone) }

  it { should belong_to(:account).class_name('WebUser') }
  it { should have_many(:enrollments) }
  it { should have_many(:classrooms).through(:enrollments) }

  describe 'when create success' do
    describe '#guid' do
      subject { model.guid }

      let(:model) { create :student }
      let(:generated_guid) do
        "#{described_class.name.upcase}-#{model.id}-#{model.created_at.to_i.to_s(36)}"
      end

      it { is_expected.to eq generated_guid }
    end
  end
end
