require 'rails_helper'

RSpec.describe Employment, type: :model do
  it { should belong_to(:teacher) }
  it { should belong_to(:classroom) }
end
