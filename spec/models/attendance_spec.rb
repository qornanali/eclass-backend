require 'rails_helper'

RSpec.describe Attendance, type: :model do
  it { should belong_to(:web_user) }
  it { should belong_to(:study_session) }
end
