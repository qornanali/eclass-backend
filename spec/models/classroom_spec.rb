require 'rails_helper'

RSpec.describe Classroom, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_uniqueness_of(:guid) }
  it { should allow_values(nil).for(:guid) }

  it { should have_many(:employments) }
  it { should have_many(:teachers).through(:employments) }
  it { should have_many(:enrollments) }
  it { should have_many(:students).through(:enrollments) }
  it { should have_many(:schedules) }
  it { should have_many(:lectures) }
  it { should have_many(:subjects).through(:lectures) }

  describe 'when create success' do
    describe '#guid' do
      subject { model.guid }

      let(:model) { create :classroom }
      let(:generated_guid) do
        "#{described_class.name.upcase}-#{model.id}-#{model.created_at.to_i.to_s(36)}"
      end

      it { is_expected.to eq generated_guid }
    end
  end
end
