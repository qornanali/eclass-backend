require 'rails_helper'

RSpec.describe StudySession, type: :model do
  it { should belong_to(:schedule) }
end
