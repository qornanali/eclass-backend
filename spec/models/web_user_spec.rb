require 'rails_helper'

RSpec.describe WebUser, type: :model do
  it { should validate_presence_of(:email) }
  it { should validate_presence_of(:password) }
  it { should validate_uniqueness_of(:email) }
  it { should validate_length_of(:password).is_at_most(72) }
  it { should_not allow_value(Faker::Lorem.characters(number: 30)).for(:email) }
  it { should allow_value(Faker::Internet.email).for(:email) }
  it { should have_one(:teacher).class_name('Teacher') }
  it { should have_one(:student).class_name('Student') }
end
