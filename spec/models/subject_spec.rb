require 'rails_helper'

RSpec.describe Subject, type: :model do
  it { should validate_presence_of(:name) }
  it { should validate_uniqueness_of(:guid) }
  it { should allow_values(nil).for(:guid) }
  it { should have_many(:lectures) }
  it { should have_many(:classrooms).through(:lectures) }

  describe 'when create success' do
    describe '#guid' do
      subject { model.guid }

      let(:model) { create :subject }
      let(:generated_guid) do
        "#{described_class.name.upcase}-#{model.id}-#{model.created_at.to_i.to_s(36)}"
      end

      it { is_expected.to eq generated_guid }
    end
  end
end
