require 'rails_helper'

RSpec.describe JsonWebToken do
  let(:login_type) { %w[teacher student].sample }

  describe '.encode' do
    let(:payload) do
      { email: Faker::Internet.email, login_type: login_type }
    end
    let(:expired_at) { rand(1..24).hours.from_now }

    it 'use JWT to generate auth-token' do
      expect(JWT).to receive(:encode)
        .with(payload.merge(expired_at: expired_at.to_i), Rails.application.secrets.secret_key_base)
      described_class.encode(payload, expired_at)
    end

    context 'when parameter expired_at not passed' do
      let(:expired_at) { 24.hours.from_now }

      it 'use JWT to generate auth-token' do
        expect(JWT).to receive(:encode)
          .with(payload.merge(expired_at: expired_at.to_i), Rails.application.secrets.secret_key_base)
        described_class.encode(payload, expired_at)
      end
    end

    context 'when error raised' do
      it 'use JWT to generate auth-token' do
        allow(JWT).to receive(:encode).and_raise(StandardError)
        expect(described_class.encode(payload, expired_at)).to eq nil
      end
    end
  end

  describe '.decode' do
    subject { described_class.decode(token) }

    let(:token) { Faker::Lorem.characters(number: 30) }
    let(:decoded) do
      [
        {
          expired_at: rand(1..60),
          email: Faker::Internet.email,
          login_type: login_type
        }
      ]
    end
    let(:payload) { HashWithIndifferentAccess.new(decoded[0]) }

    context 'when error raised' do
      before do
        allow(JWT).to receive(:decode)
          .with(token, Rails.application.secrets.secret_key_base).and_return(decoded)
      end

      it { is_expected.to eq payload }
    end

    context 'when error raised' do
      before do
        allow(JWT).to receive(:encode).and_raise(StandardError)
      end

      it { is_expected.to eq nil }
    end
  end
end
