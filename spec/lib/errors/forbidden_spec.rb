require 'rails_helper'

RSpec.describe Errors::Forbidden do
  describe '#message' do
    subject { described_class.new(message).message }

    context 'with message in param' do
      let(:message) { Faker::Lorem.sentence }

      it { is_expected.to eq message }

      context 'with message is nil' do
        let(:message) { nil }
        let(:forbidden_message) { I18n.t('errors.forbidden') }

        it { is_expected.to eq forbidden_message }
      end
    end
  end
end
