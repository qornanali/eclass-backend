require 'rails_helper'

RSpec.describe Errors::Unauthorized do
  describe '#message' do
    subject { described_class.new(message).message }

    context 'with message in param' do
      let(:message) { Faker::Lorem.sentence }

      it { is_expected.to eq message }

      context 'with message is nil' do
        let(:message) { nil }
        let(:unauthorized_message) { I18n.t('errors.unauthorized') }

        it { is_expected.to eq unauthorized_message }
      end
    end
  end
end
