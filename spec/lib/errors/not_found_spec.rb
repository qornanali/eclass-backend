require 'rails_helper'

RSpec.describe Errors::NotFound do
  describe '#message' do
    subject { described_class.new(message).message }

    context 'with message in param' do
      let(:message) { Faker::Lorem.sentence }

      it { is_expected.to eq message }

      context 'with message is nil' do
        let(:message) { nil }
        let(:not_found_message) { I18n.t('errors.not_found') }

        it { is_expected.to eq not_found_message }
      end
    end
  end
end
