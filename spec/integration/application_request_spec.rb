require 'rails_helper'

RSpec.describe ApplicationController, type: :request do
  describe 'GET /_ping' do
    it 'returns http_status ok' do
      get '/_ping'
      expect(response).to have_http_status(200)
    end
  end
end
