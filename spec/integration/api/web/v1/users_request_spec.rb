require 'rails_helper'

RSpec.describe Api::Web::V1::UsersController, type: :request do
  let(:base_url) { '/web/v1/user' }
  let(:base_result) do
    {
      message: message,
      data: data
    }.as_json
  end
  let(:message) { '' }
  let(:data) { nil }

  describe 'POST /web/v1/user' do
    let(:url) { base_url }
    let(:account) { attributes_for :web_user }

    context 'with invalid params' do
      context 'with registration type is not included in the array' do
        let(:params) { { registration_type: Faker::Lorem.word } }

        it 'fails to create the user' do
          post url, params: params
          expect(response).to have_http_status :bad_request
        end
      end
    end

    context 'when create user for teacher' do
      let(:teacher) do
        attributes_for(:teacher).except(:account).merge(registration_type: 'teacher')
      end
      let(:params) { teacher.merge(account) }

      context 'with valid params' do
        it 'success create the user' do
          post url, params: params
          expect(response).to have_http_status :ok
          json_body = JSON.parse(response.body)
          expect(json_body['data']).not_to eq nil
          web_user = WebUser.last
          expect(web_user.email).to eq params[:email]
          teacher = Teacher.last
          expect(teacher.full_name).to eq params[:full_name]
        end
      end
    end

    context 'when create user for student' do
      let(:student) do
        attributes_for(:student).except(:account).merge(registration_type: 'student')
      end
      let(:params) { student.merge(account) }

      context 'with valid params' do
        it 'success create the user' do
          post url, params: params
          expect(response).to have_http_status :ok
          json_body = JSON.parse(response.body)
          expect(json_body['data']).not_to eq nil
          web_user = WebUser.last
          expect(web_user.email).to eq params[:email]
          student = Student.last
          expect(student.full_name).to eq params[:full_name]
        end
      end
    end
  end
end
