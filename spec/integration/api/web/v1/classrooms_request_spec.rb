require 'rails_helper'

RSpec.describe Api::Web::V1::ClassroomsController, type: :request do
  let(:base_url) { '/web/v1/classrooms' }

  let(:auth_data) do
    {
      email: email,
      expired_at: expired_at,
      login_type: login_type
    }
  end
  let(:expired_at) { 24.hours.from_now }

  before do
    allow(JsonWebToken).to receive(:decode).and_return(auth_data)
  end

  describe 'GET /web/v1/classrooms' do
    let(:url) { base_url }

    context 'when accessed by teacher' do
      let!(:classroom) { create_list(:classroom, item_count) }
      let(:item_count) { rand(2..3) }
      let(:teacher) { create :teacher }
      let(:email) { teacher.account.email }
      let(:login_type) { 'teacher' }

      context 'when headers is valid' do
        it 'success get the classrooms data' do
          get url, headers: web_headers
          expect(response).to have_http_status :ok
          json_body = JSON.parse(response.body)
          expect(json_body['data']).not_to eq nil
          expect(json_body['data'].size).to eq item_count
        end
      end
    end
  end

  describe 'POST /web/v1/classrooms' do
    let(:url) { base_url }
    let(:params) { attributes_for :classroom }

    context 'when accessed by student' do
      let(:student) { create :student }
      let(:email) { student.account.email }
      let(:login_type) { 'student' }

      it 'fails to create classroom' do
        post url, params: params, headers: web_headers
        expect(response).to have_http_status :forbidden
      end
    end

    context 'when accessed by teacher' do
      let(:teacher) { create :teacher }
      let(:email) { teacher.account.email }
      let(:login_type) { 'teacher' }

      context 'when token cannot be decoded' do
        let(:auth_data) { nil }

        it 'fails to create classroom' do
          post url, params: params, headers: web_headers
          expect(response).to have_http_status :unauthorized
        end
      end

      context 'when token is expired' do
        let(:expired_at) { Time.now - 1.hour }

        it 'fails to create classroom' do
          post url, params: params, headers: web_headers
          expect(response).to have_http_status :unauthorized
        end
      end

      context 'when auth-data is invalid' do
        context 'with user not found' do
          let(:email) { Faker::Internet.email }

          it 'fails to create classroom' do
            post url, params: params, headers: web_headers
            expect(response).to have_http_status :unauthorized
          end
        end
      end

      context 'when headers is valid' do
        context 'when params is valid' do
          it 'success create the classroom' do
            post url, params: params, headers: web_headers
            expect(response).to have_http_status :ok
            json_body = JSON.parse(response.body)
            expect(json_body['data']).not_to eq nil
            classroom = Classroom.last
            expect(classroom.name).to eq params[:name]
            employment = Employment.last
            expect(employment.teacher_id).to eq teacher.id
          end
        end
      end
    end
  end

  context 'with teacher features' do
    let(:teacher) { create :teacher }
    let(:email) { teacher.account.email }
    let(:login_type) { 'teacher' }

    describe 'POST /web/v1/classrooms/{guid}/apply' do
      let(:url) { "#{base_url}/#{classroom_guid}/apply" }
      let(:classroom_guid) { classroom.guid }
      let(:classroom) { create :classroom }

      context 'when classroom_guid is invalid' do
        let(:classroom_guid) { Faker::Lorem.word }

        it 'fails to apply to the classroom' do
          post url, headers: web_headers
          expect(response).to have_http_status :not_found
        end
      end

      context 'when accessed by student' do
        let(:student) { create :student }
        let(:email) { student.account.email }
        let(:login_type) { 'student' }

        it 'fails to apply to the classroom' do
          post url, headers: web_headers
          expect(response).to have_http_status :forbidden
        end
      end

      context 'with valid params' do
        let(:item_count) { rand(2..3) }

        it 'success apply to the classroom' do
          post url, headers: web_headers
          expect(response).to have_http_status :ok
          json_body = JSON.parse(response.body)
          expect(json_body['data']).not_to eq nil
          employment = Employment.last
          expect(employment.teacher_id).to eq teacher.id
        end
      end
    end

    describe 'POST /web/v1/classrooms/{guid}/resign' do
      let(:url) { "#{base_url}/#{classroom_guid}/resign" }
      let(:classroom_guid) { classroom.guid }
      let(:classroom) { create :classroom }

      context 'when classroom_guid is invalid' do
        let(:classroom_guid) { Faker::Lorem.word }

        it 'fails to resign from the classroom' do
          post url, headers: web_headers
          expect(response).to have_http_status :not_found
        end
      end

      context 'when accessed by student' do
        let(:student) { create :student }
        let(:email) { student.account.email }
        let(:login_type) { 'student' }

        it 'fails to resign from the classroom' do
          post url, headers: web_headers
          expect(response).to have_http_status :forbidden
        end
      end

      context 'with valid params' do
        let(:item_count) { rand(2..3) }
        let!(:employment) { create :employment, teacher: teacher, classroom: classroom }

        it 'success resign from the classroom' do
          post url, headers: web_headers
          expect(response).to have_http_status :ok
          json_body = JSON.parse(response.body)
          expect(json_body['data']).not_to eq nil
          employment = classroom.employments.find_by(teacher: teacher)
          expect(employment).to eq nil
        end
      end
    end
  end

  context 'with student features' do
    let(:student) { create :student }
    let(:email) { student.account.email }
    let(:login_type) { 'student' }

    describe 'POST /web/v1/classrooms/{guid}/enroll' do
      let(:url) { "#{base_url}/#{classroom_guid}/enroll" }
      let(:classroom_guid) { classroom.guid }
      let(:classroom) { create :classroom }

      context 'when classroom_guid is invalid' do
        let(:classroom_guid) { Faker::Lorem.word }

        it 'fails to enroll to the classroom' do
          post url, headers: web_headers
          expect(response).to have_http_status :not_found
        end
      end

      context 'when accessed by teacher' do
        let(:teacher) { create :teacher }
        let(:email) { teacher.account.email }
        let(:login_type) { 'teacher' }

        it 'fails to enroll to the classroom' do
          post url, headers: web_headers
          expect(response).to have_http_status :forbidden
        end
      end

      context 'with valid params' do
        let(:item_count) { rand(2..3) }

        it 'success enroll to the classroom' do
          post url, headers: web_headers
          expect(response).to have_http_status :ok
          json_body = JSON.parse(response.body)
          expect(json_body['data']).not_to eq nil
          enrollment = Enrollment.last
          expect(enrollment.student_id).to eq student.id
        end
      end
    end

    describe 'POST /web/v1/classrooms/{guid}/leave' do
      let(:url) { "#{base_url}/#{classroom_guid}/leave" }
      let(:classroom_guid) { classroom.guid }
      let(:classroom) { create :classroom }

      context 'when classroom_guid is invalid' do
        let(:classroom_guid) { Faker::Lorem.word }

        it 'fails to leave the classroom' do
          post url, headers: web_headers
          expect(response).to have_http_status :not_found
        end
      end

      context 'when accessed by teacher' do
        let(:teacher) { create :teacher }
        let(:email) { teacher.account.email }
        let(:login_type) { 'teacher' }

        it 'fails to leave the classroom' do
          post url, headers: web_headers
          expect(response).to have_http_status :forbidden
        end
      end

      context 'with valid params' do
        let(:item_count) { rand(2..3) }
        let!(:enrollment) { create :enrollment, student: student, classroom: classroom }

        it 'success leave the classroom' do
          post url, headers: web_headers
          expect(response).to have_http_status :ok
          json_body = JSON.parse(response.body)
          expect(json_body['data']).not_to eq nil
          enrollment = classroom.enrollments.find_by(student: student)
          expect(enrollment).to eq nil
        end
      end
    end
  end
end
