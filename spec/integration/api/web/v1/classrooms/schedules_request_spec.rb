require 'rails_helper'

RSpec.describe Api::Web::V1::Classrooms::SchedulesController, type: :request do
  let(:super_url) { '/web/v1/classrooms' }
  let(:base_url) { "#{super_url}/#{classroom_guid}/schedules" }

  let(:auth_data) do
    {
      email: email,
      expired_at: expired_at,
      login_type: login_type
    }
  end
  let(:teacher) { create :teacher }
  let(:expired_at) { 24.hours.from_now }
  let(:email) { teacher.account.email }
  let(:login_type) { 'teacher' }

  before do
    allow(JsonWebToken).to receive(:decode).and_return(auth_data)
  end

  describe 'GET /web/v1/classrooms/{guid}/schedules' do
    let(:url) { base_url }

    context 'when classroom_guid is invalid' do
      let(:classroom_guid) { Faker::Lorem.word }

      it 'fails get the schedules data' do
        get url, headers: web_headers
        expect(response).to have_http_status :not_found
      end
    end

    context 'with valid params' do
      let(:classroom_guid) { classroom.guid }
      let(:classroom) { create :classroom }
      let!(:schedules) { create_list :schedule, item_count, classroom: classroom }
      let(:item_count) { rand(2..3) }

      it 'success get the schedule data' do
        get url, headers: web_headers
        expect(response).to have_http_status :ok
        json_body = JSON.parse(response.body)
        expect(json_body['data']).not_to eq nil
        expect(json_body['data'].size).to eq item_count
      end
    end

    describe 'POST /web/v1/classrooms/{guid}/schedules' do
      let(:url) { base_url }

      context 'with valid params' do
        let(:classroom_guid) { classroom.guid }
        let(:classroom) { create :classroom }
        let!(:schedule) { attributes_for :schedule }
        let(:params) { schedule }

        it 'success get the classrooms data' do
          post url, params: params, headers: web_headers
          expect(response).to have_http_status :ok
          json_body = JSON.parse(response.body)
          expect(json_body['data']).not_to eq nil
          schedule = Schedule.last
          expect(schedule.classroom_id).to eq classroom.id
          expect(schedule.day).to eq params[:day]
          expect(schedule.time).to eq params[:time]
          expect(schedule.duration).to eq params[:duration]
        end
      end
    end
  end
end
