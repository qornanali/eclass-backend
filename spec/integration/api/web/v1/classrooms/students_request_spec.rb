require 'rails_helper'

RSpec.describe Api::Web::V1::Classrooms::StudentsController, type: :request do
  let(:super_url) { '/web/v1/classrooms' }
  let(:base_url) { "#{super_url}/#{classroom_guid}/students" }

  let(:auth_data) do
    {
      email: email,
      expired_at: expired_at,
      login_type: login_type
    }
  end
  let(:student) { create :student }
  let(:expired_at) { 24.hours.from_now }
  let(:email) { student.account.email }
  let(:login_type) { 'student' }

  before do
    allow(JsonWebToken).to receive(:decode).and_return(auth_data)
  end

  describe 'GET /web/v1/classrooms/{guid}/students' do
    let(:url) { base_url }

    context 'when classroom_guid is invalid' do
      let(:classroom_guid) { Faker::Lorem.word }

      it 'fails get the students data' do
        get url, headers: web_headers
        expect(response).to have_http_status :not_found
      end
    end

    context 'with valid params' do
      let(:classroom_guid) { classroom.guid }
      let(:classroom) { create :classroom }
      let!(:enrollments) { create_list :enrollment, item_count, classroom: classroom }
      let(:item_count) { rand(2..3) }

      it 'success get the students data' do
        get url, headers: web_headers
        expect(response).to have_http_status :ok
        json_body = JSON.parse(response.body)
        expect(json_body['data']).not_to eq nil
        expect(json_body['data'].size).to eq item_count
      end
    end
  end
end
