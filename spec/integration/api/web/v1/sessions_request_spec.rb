require 'rails_helper'

RSpec.describe Api::Web::V1::SessionsController, type: :request do
  let(:base_url) { '/web/v1/session' }

  describe 'POST /web/v1/session' do
    let(:url) { base_url }
    let(:params) do
      {
        email: web_user.email,
        password: password,
        login_type: login_type
      }
    end
    let(:web_user) { create :web_user, password: password }
    let(:password) { Faker::Lorem.characters(number: 30) }
    let(:login_type) { %w[teacher student].sample }

    context 'with invalid params' do
      context 'with invalid login_type' do
        let(:login_type) { Faker::Lorem.word }

        it 'failed to create the session' do
          post url, params: params
          expect(response).to have_http_status :bad_request
        end
      end
    end

    context 'with valid params' do
      context 'with login_type is teacher' do
        let(:login_type) { 'teacher' }

        context 'when teacher data is not present' do
          it 'failed to create the session' do
            post url, params: params
            expect(response).to have_http_status :unauthorized
          end
        end

        context 'when teacher data is present' do
          let!(:teacher) { create :teacher, account: web_user }

          it 'success create a session' do
            post url, params: params
            expect(response).to have_http_status :ok
            json_body = JSON.parse(response.body)
            expect(json_body['data']['auth_token']).to_not eq nil
          end
        end
      end

      context 'with login_type is student' do
        let(:login_type) { 'student' }

        context 'when student data is not present' do
          it 'failed to create the session' do
            post url, params: params
            expect(response).to have_http_status :unauthorized
          end
        end

        context 'when teacher data is present' do
          let!(:student) { create :student, account: web_user }

          it 'success create a session' do
            post url, params: params
            expect(response).to have_http_status :ok
            json_body = JSON.parse(response.body)
            expect(json_body['data']['auth_token']).to_not eq nil
          end
        end
      end
    end
  end
end
