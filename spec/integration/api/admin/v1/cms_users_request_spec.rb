require 'rails_helper'

RSpec.describe Api::Admin::V1::CmsUsersController, type: :request do
  let(:base_url) { '/admin/v1/cms_users' }
  let(:base_result) do
    {
      message: message,
      data: data
    }.as_json
  end
  let(:message) { '' }
  let(:data) { nil }

  describe 'POST /admin/v1/cms_users' do
    let(:url) { base_url }
    let(:params) { attributes_for(:cms_user) }

    context 'with invalid headers' do
      it 'fails to create the cms_user' do
        post url, headers: nil, params: params
        expect(response).to have_http_status :unauthorized
      end
    end

    context 'with invalid params' do
      context 'when parameter client_id is missing' do
        it 'fails to create the cms_user' do
          post url, headers: admin_headers, params: params.except(:client_id)
          expect(response).to have_http_status :bad_request
        end
      end
    end

    context 'with valid headers' do
      context 'with valid params' do
        let(:expected_result) { base_result }
        let(:message) { Response::SUCCEED_DEFAULT_MESSAGE }
        let(:password_hash) { Faker::Lorem.characters(number: 100) }

        before do
          allow(BCrypt::Password).to receive(:create).with(params[:password], cost: 4)
                                                     .and_return(password_hash)
        end

        it 'success create a cms_user' do
          post url, headers: admin_headers, params: params
          expect(response).to have_http_status :ok
          expect(JSON.parse(response.body)).to eq expected_result
          cms_user = CmsUser.last
          expect(cms_user.client_id).to eq params[:client_id]
          expect(cms_user.password_digest).to eq password_hash
        end
      end
    end
  end
end
