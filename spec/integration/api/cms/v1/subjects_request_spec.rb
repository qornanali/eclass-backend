require 'rails_helper'

RSpec.describe Api::Cms::V1::SubjectsController, type: :request do
  let(:base_url) { '/cms/v1/subjects' }

  describe 'POST /cms/v1/subjects' do
    let(:url) { base_url }
    let(:params) { attributes_for(:subject) }

    context 'with invalid headers' do
      it 'fails to create the subject' do
        post url, headers: nil, params: params
        expect(response).to have_http_status :unauthorized
      end
    end

    context 'with invalid params' do
      context 'when parameter name is missing' do
        it 'fails to create the subject' do
          post url, headers: cms_headers, params: params.except(:name)
          expect(response).to have_http_status :bad_request
        end
      end
    end

    context 'with valid headers' do
      context 'with valid params' do
        it 'success create a subject' do
          post url, headers: cms_headers, params: params
          expect(response).to have_http_status :ok
          json_body = JSON.parse(response.body)
          expect(json_body['data']).not_to eq nil
          subject = Subject.last
          expect(subject.name).to eq params[:name]
        end
      end
    end
  end

  describe 'GET /cms/v1/subjects' do
    let(:url) { base_url }
    let!(:subjects) { create_list(:subject, item_count) }
    let(:item_count) { rand(1..3) }

    context 'with valid headers' do
      context 'with valid params' do
        it 'success get subjects' do
          get url, headers: cms_headers
          expect(response).to have_http_status :ok
          json_body = JSON.parse(response.body)
          expect(json_body['data'].size).to eq item_count
        end
      end
    end
  end

  describe 'GET /cms/v1/subjects/{guid}' do
    let(:url) { "#{base_url}/#{subject_guid}" }
    let(:subject_guid) { subject.guid }
    let(:subject) { create(:subject) }

    context 'with valid headers' do
      context 'with valid params' do
        it 'success get the subject' do
          get url, headers: cms_headers
          expect(response).to have_http_status :ok
          json_body = JSON.parse(response.body)
          expect(json_body['data']['guid']).to eq subject_guid
        end
      end

      context 'with invalid params' do
        let(:subject_guid) { Faker::Lorem.word }

        context 'with invalid guid' do
          it 'failed to patch the subject' do
            get url, headers: cms_headers
            expect(response).to have_http_status :not_found
          end
        end
      end
    end
  end

  describe 'PATCH /cms/v1/subjects/{guid}' do
    let(:url) { "#{base_url}/#{subject_guid}" }
    let(:subject_guid) { subject.guid }
    let(:subject) { create(:subject) }
    let(:params) { { name: name } }
    let(:name) { Faker::Lorem.word }

    context 'with valid headers' do
      context 'with valid params' do
        context 'when patching field name' do
          it 'success patch the subject' do
            patch url, headers: cms_headers, params: params
            expect(response).to have_http_status :ok
            json_body = JSON.parse(response.body)
            expect(json_body['data']).not_to eq nil
            subject = Subject.last
            expect(subject.name).to eq params[:name]
          end
        end
      end

      context 'with invalid params' do
        context 'with invalid guid' do
          let(:subject_guid) { Faker::Lorem.word }

          it 'returns not_found' do
            patch url, headers: cms_headers, params: params
            expect(response).to have_http_status :not_found
          end
        end

        context 'when parameter name is nil' do
          let(:name) { nil }

          it 'fails to patch the subject' do
            patch url, headers: cms_headers, params: params
            expect(response).to have_http_status :bad_request
          end
        end
      end
    end
  end

  describe 'DELETE /cms/v1/subjects/{guid}' do
    let(:url) { "#{base_url}/#{subject_guid}" }
    let(:subject_guid) { subject.guid }
    let(:subject) { create(:subject) }

    context 'with valid headers' do
      context 'with valid params' do
        it 'success delete the subject' do
          delete url, headers: cms_headers
          expect(response).to have_http_status :ok
          json_body = JSON.parse(response.body)
          expect(json_body['data']).not_to eq nil
          subject = Subject.find_by_guid(subject_guid)
          expect(subject).to eq nil
        end
      end

      context 'with invalid params' do
        context 'with invalid guid' do
          let(:subject_guid) { Faker::Lorem.word }

          it 'returns not_found' do
            patch url, headers: cms_headers
            expect(response).to have_http_status :not_found
          end
        end
      end
    end
  end
end
