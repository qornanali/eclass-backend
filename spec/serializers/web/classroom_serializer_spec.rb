require 'rails_helper'

RSpec.describe Web::ClassroomSerializer do
  include_examples 'check json schema', 'web/v1/classroom', :classroom, {}, true
end
