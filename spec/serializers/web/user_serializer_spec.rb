require 'rails_helper'

RSpec.describe Web::UserSerializer do
  include_examples 'check json schema', 'web/v1/user', :web_user, {}, true
end
