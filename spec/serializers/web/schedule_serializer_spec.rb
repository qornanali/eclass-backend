require 'rails_helper'

RSpec.describe Web::ScheduleSerializer do
  include_examples 'check json schema', 'web/v1/schedule', :schedule, {}, true
end
