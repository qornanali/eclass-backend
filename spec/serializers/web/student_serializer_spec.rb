require 'rails_helper'

RSpec.describe Web::StudentSerializer do
  include_examples 'check json schema', 'web/v1/student', :student, {}, true
end
