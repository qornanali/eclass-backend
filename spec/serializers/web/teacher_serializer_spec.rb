require 'rails_helper'

RSpec.describe Web::TeacherSerializer do
  include_examples 'check json schema', 'web/v1/teacher', :teacher, {}, true
end
