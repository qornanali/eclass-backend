require 'rails_helper'

RSpec.describe Cms::SubjectSerializer do
  include_examples 'check json schema', 'cms/v1/subject', :subject, { guid: 'SUBJECT-1-xxyy' }, true
end
