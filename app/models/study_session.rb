class StudySession < ApplicationRecord
  belongs_to :schedule
  has_many :attendances
end
