class Attendance < ApplicationRecord
  belongs_to :study_session
  belongs_to :web_user
end
