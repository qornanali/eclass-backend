class Response
  SUCCEED_DEFAULT_MESSAGE = 'succeed'.freeze
  FAILED_DEFAULT_MESSAGE = 'failed'.freeze

  def initialize(data: nil, message: '')
    @data = data
    @message = message
  end

  def self.succeed(data: nil, message: SUCCEED_DEFAULT_MESSAGE)
    new(data: data, message: message)
  end

  def self.failed(message: FAILED_DEFAULT_MESSAGE)
    new(message: message)
  end
end
