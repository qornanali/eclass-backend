class WebUser < ApplicationRecord
  validates_presence_of :email, :password
  validates_uniqueness_of :email
  validates_length_of :password, maximum: 72
  validates_format_of :email, with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i

  has_secure_password

  has_one :teacher, class_name: 'Teacher', foreign_key: 'web_users_id'
  has_one :student, class_name: 'Student', foreign_key: 'web_users_id'
end
