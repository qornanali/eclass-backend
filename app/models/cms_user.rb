class CmsUser < ApplicationRecord
  validates_presence_of :client_id, :password, :name
  validates_uniqueness_of :client_id
  validates_length_of :password, maximum: 72

  has_secure_password
end
