class Schedule < ApplicationRecord
  include GuidGenerator

  TIME_REGEX = /[\d]{1,2}:[\d]{1,2}/.freeze

  validates_presence_of :time, :day, :duration
  validates_uniqueness_of :guid, allow_nil: true
  validates :duration, numericality: { only_integer: true }
  validates :day, inclusion: { in: Date::DAYNAMES.map(&:downcase) }
  validate :time_should_have_correct_value, if: -> { time.present? }
  validates_format_of :time, with: TIME_REGEX

  belongs_to :classroom
  has_many :study_sessions

  private

  def time_should_have_correct_value
    time_items = time.split(':')
    hour = time_items[0].to_i
    minute = time_items[1].to_i
    errors.add(:time, I18n.t('errors.invalid_value.hour')) if hour.negative? || hour > 23
    errors.add(:time, I18n.t('errors.invalid_value.minute')) if minute.negative? || minute > 59
  end
end
