class Teacher < ApplicationRecord
  include GuidGenerator

  ALLOWED_GENDERS = %w[male female].freeze

  validates_presence_of :full_name, :phone
  validates_uniqueness_of :guid, allow_nil: true
  validates_length_of :bio, maximum: 100
  validates_length_of :full_name, maximum: 71
  validates :gender, inclusion: { in: ALLOWED_GENDERS }

  belongs_to :account, class_name: 'WebUser', foreign_key: 'web_users_id'
  has_many :employments
  has_many :classrooms, through: :employments
end
