class Subject < ApplicationRecord
  include GuidGenerator

  validates_presence_of :name
  validates_uniqueness_of :guid, allow_nil: true
  has_many :lectures
  has_many :classrooms, through: :lectures
end
