class Classroom < ApplicationRecord
  include GuidGenerator

  validates_presence_of :name
  validates_uniqueness_of :guid, allow_nil: true

  has_many :employments
  has_many :teachers, through: :employments
  has_many :schedules
  has_many :enrollments
  has_many :students, through: :enrollments
  has_many :lectures
  has_many :subjects, through: :lectures
end
