class WebUserSession < ApplicationRecord
  validates_presence_of :auth_token, :expired_at
end
