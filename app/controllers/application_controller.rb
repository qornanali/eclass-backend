class ApplicationController < ActionController::API
  def ping
    render json: {}, status: :ok
  end
end
