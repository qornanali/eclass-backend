module Api
  module Concerns
    module Renderer
      extend ActiveSupport::Concern

      def render_ok(data, serializer = nil)
        render json: Response.succeed(data: serialize_resources(data, serializer))
      end

      private

      def serialize_resources(resources, serializer)
        return nil unless resources

        return resources.map { |resource| serialize_resource(resource, serializer) } if resources.is_a?(Enumerable)

        serialize_resource(resources, serializer)
      end

      def serialize_resource(resource, serializer)
        serializer_class = serializer || ActiveModel::Serializer.serializer_for(resource)
        return resource unless serializer_class

        serialized_resource = serializer_class.new(resource)
        adapter = ActiveModelSerializers::Adapter.configured_adapter.new(serialized_resource)
        adapter.serializable_hash
      end
    end
  end
end
