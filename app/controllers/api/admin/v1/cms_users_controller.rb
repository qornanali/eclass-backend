module Api
  module Admin
    module V1
      class CmsUsersController < AdminController
        def create
          CmsUser.create!(create_params)
          render json: Response.succeed
        end

        private

        def create_params
          params.permit(:name, :client_id, :password)
        end
      end
    end
  end
end
