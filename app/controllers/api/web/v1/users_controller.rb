module Api
  module Web
    module V1
      class UsersController < BaseController
        TEACHER = 'teacher'.freeze
        STUDENT = 'student'.freeze
        REGISTRATION_TYPES = [TEACHER, STUDENT].freeze

        def create
          validate_params
          create_user
          render_ok(@data, ::Web::TeacherSerializer)
        end

        private

        def validate_params
          return if REGISTRATION_TYPES.include?(registration_type)

          raise Errors::BadRequest, I18n.t('errors.web_user.invalid_registration')
        end

        def registration_type
          params.require(:registration_type)
        end

        def create_user
          ActiveRecord::Base.transaction do
            user = WebUser.create!(user_params)
            @data = if register_as_teacher?
                      Teacher.create!(teacher_params.merge(account: user))
                    else
                      Student.create!(student_params.merge(account: user))
                    end
          end
        end

        def user_params
          params.permit(:email, :password)
        end

        def register_as_teacher?
          registration_type == TEACHER
        end

        def teacher_params
          params.permit(:full_name, :bio, :gender, :phone)
        end

        def student_params
          params.permit(:full_name, :bio, :gender, :phone)
        end
      end
    end
  end
end
