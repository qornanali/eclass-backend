module Api
  module Web
    module V1
      class ClassroomsController < WebController
        def create
          validate_teacher_feature
          ActiveRecord::Base.transaction do
            @data = Classroom.create!(create_classroom_params)

            Employment.create!(create_employment_params)
          end
          render_ok(@data, ::Web::ClassroomSerializer)
        end

        def index
          data = Classroom.all
          render_ok(data, ::Web::ClassroomSerializer)
        end

        def apply
          validate_teacher_feature
          employment = Employment.find_or_create_by(employment_params)
          data = employment.teacher
          render_ok(data, ::Web::StudentSerializer)
        end

        def resign
          validate_teacher_feature
          employment = Employment.find_by!(employment_params)
          employment.delete
          data = @user.teacher
          render_ok(data, ::Web::StudentSerializer)
        end

        def enroll
          validate_student_feature
          enrollment = Enrollment.find_or_create_by(enrollment_params)
          data = enrollment.student
          render_ok(data, ::Web::StudentSerializer)
        end

        def leave
          validate_student_feature
          enrollment = Enrollment.find_by!(enrollment_params)
          enrollment.delete
          data = @user.student
          render_ok(data, ::Web::StudentSerializer)
        end

        private

        def create_classroom_params
          params.permit(:name, :description)
        end

        def login_type
          @auth_data[:login_type]
        end

        def create_employment_params
          { teacher: @user.teacher, classroom: @data }
        end

        def validate_teacher_feature
          return if login_type == 'teacher'

          raise Errors::Forbidden
        end

        def employment_params
          { teacher: @user.teacher, classroom: classroom }
        end

        def validate_student_feature
          return if login_type == 'student'

          raise Errors::Forbidden
        end

        def enrollment_params
          { student: @user.student, classroom: classroom }
        end

        def classroom
          @classroom ||= Classroom.find_by_guid!(params[:classroom_guid])
        end
      end
    end
  end
end
