module Api
  module Web
    module V1
      module Classrooms
        class StudentsController < WebController
          def index
            classroom = Classroom.find_by_guid!(params[:classroom_guid])
            data = classroom.students
            render_ok(data, ::Web::StudentSerializer)
          end
        end
      end
    end
  end
end
