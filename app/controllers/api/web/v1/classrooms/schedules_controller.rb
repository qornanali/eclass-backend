module Api
  module Web
    module V1
      module Classrooms
        class SchedulesController < WebController
          def index
            classroom = Classroom.find_by_guid!(params[:classroom_guid])
            data = classroom.schedules
            render_ok(data, ::Web::ScheduleSerializer)
          end

          def create
            classroom = Classroom.find_by_guid!(params[:classroom_guid])
            data = Schedule.create!(create_params.merge(classroom: classroom))
            render_ok(data, ::Web::ScheduleSerializer)
          end

          private

          def create_params
            params.permit(:day, :time, :duration)
          end
        end
      end
    end
  end
end
