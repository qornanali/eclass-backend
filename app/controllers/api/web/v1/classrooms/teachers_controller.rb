module Api
  module Web
    module V1
      module Classrooms
        class TeachersController < WebController
          def index
            classroom = Classroom.find_by_guid!(params[:classroom_guid])
            data = classroom.teachers
            render_ok(data, ::Web::TeacherSerializer)
          end
        end
      end
    end
  end
end
