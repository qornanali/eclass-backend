module Api
  module Web
    module V1
      class SessionsController < BaseController
        TEACHER = 'teacher'.freeze
        STUDENT = 'student'.freeze
        LOGIN_TYPES = [TEACHER, STUDENT].freeze

        def create
          @web_user = WebUser.find_by(email: pr_email)
          validate_params

          data = { auth_token: JsonWebToken.encode(session_params) }
          render json: Response.succeed(data: data)
        end

        private

        def pr_email
          params[:email]
        end

        def validate_params
          unless LOGIN_TYPES.include?(login_type)
            raise Errors::BadRequest, I18n.t('errors.web_user.invalid_registration')
          end
          return if @web_user&.authenticate(pr_password) && valid_login_data?

          raise Errors::Unauthorized
        end

        def login_type
          params.require(:login_type)
        end

        def valid_login_data?
          (login_as_student? && @web_user.student) || (login_as_teacher? && @web_user.teacher)
        end

        def pr_password
          params[:password]
        end

        def login_as_student?
          login_type == STUDENT
        end

        def login_as_teacher?
          login_type == TEACHER
        end

        def session_params
          { email: pr_email, login_type: login_type }
        end
      end
    end
  end
end
