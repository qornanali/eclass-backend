module Api
  class BaseController < ApplicationController
    include Concerns::Renderer

    rescue_from Errors::Unauthorized,
                with: :unauthorized

    rescue_from ActionController::ParameterMissing,
                ActiveRecord::RecordInvalid,
                Errors::BadRequest,
                with: :bad_request

    rescue_from ActiveRecord::RecordNotFound,
                Errors::NotFound,
                with: :not_found

    rescue_from Errors::Forbidden,
                with: :forbidden

    private

    def unauthorized(error)
      render_error(error, :unauthorized)
    end

    def bad_request(error)
      render_error(error, :bad_request)
    end

    def not_found(error)
      render_error(error, :not_found)
    end

    def forbidden(error)
      render_error(error, :forbidden)
    end

    def render_error(error, status)
      render json: Response.failed(message: error.message),
             status: status
    end
  end
end
