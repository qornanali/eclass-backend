module Api
  class WebController < BaseController
    include ActionController::HttpAuthentication::Token::ControllerMethods

    before_action :authenticate

    private

    def authenticate
      authenticate_with_http_token do |token, _options|
        @auth_data = JsonWebToken.decode(token)
        validate_decoded_token

        @user = WebUser.find_by(email: @auth_data[:email])
        return if @user
      end
      raise Errors::Unauthorized
    end

    def validate_decoded_token
      return if @auth_data && token_not_expired?

      raise Errors::Unauthorized
    end

    def token_not_expired?
      Time.now.to_i <= @auth_data[:expired_at].to_i
    end
  end
end
