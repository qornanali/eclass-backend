module Api
  class AdminController < BaseController
    include ActionController::HttpAuthentication::Basic::ControllerMethods

    before_action :authenticate

    private

    def authenticate
      authenticate_with_http_basic do |username, password|
        env = Figaro.env
        return if username == env.ADMIN_ID && password == env.ADMIN_SECRET
      end
      raise Errors::Unauthorized
    end
  end
end
