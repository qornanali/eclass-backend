module Api
  class CmsController < BaseController
    include ActionController::HttpAuthentication::Basic::ControllerMethods

    before_action :authenticate

    private

    def authenticate
      authenticate_with_http_basic do |username, password|
        @user = CmsUser.find_by(client_id: username)
        return if @user&.authenticate(password)
      end
      raise Errors::Unauthorized
    end
  end
end
