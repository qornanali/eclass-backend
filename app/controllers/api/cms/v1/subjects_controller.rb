module Api
  module Cms
    module V1
      class SubjectsController < CmsController
        def create
          data = Subject.create!(create_params)
          render_ok(data, ::Cms::SubjectSerializer)
        end

        def index
          data = Subject.all
          render_ok(data, ::Cms::SubjectSerializer)
        end

        def show
          data = Subject.find_by_guid!(params[:guid])
          render_ok(data, ::Cms::SubjectSerializer)
        end

        def update
          data = Subject.find_by_guid!(params[:guid])
          data.update!(update_params)
          render_ok(data, ::Cms::SubjectSerializer)
        end

        def destroy
          data = Subject.find_by_guid!(params[:guid])
          data.delete
          render_ok(data, ::Cms::SubjectSerializer)
        end

        private

        def create_params
          params.permit(:name)
        end

        def update_params
          params.permit(:name)
        end
      end
    end
  end
end
