module Errors
  class NotFound < StandardError
    def initialize(message = nil)
      super(message || I18n.t('errors.not_found'))
    end
  end
end
