module Errors
  class Forbidden < StandardError
    def initialize(message = nil)
      super(message || I18n.t('errors.forbidden'))
    end
  end
end
