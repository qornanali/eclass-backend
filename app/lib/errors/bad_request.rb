module Errors
  class BadRequest < StandardError
    def initialize(message = nil)
      super(message || I18n.t('errors.bad_request'))
    end
  end
end
