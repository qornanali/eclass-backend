module Errors
  class Unauthorized < StandardError
    def initialize(message = nil)
      super(message || I18n.t('errors.unauthorized'))
    end
  end
end
