module Errors
  class InternalServerError < StandardError
    def initialize(message = nil)
      super(message || I18n.t('errors.internal_server_error'))
    end
  end
end
