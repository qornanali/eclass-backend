class JsonWebToken
  def self.encode(payload, expired_at = 24.hours.from_now)
    payload[:expired_at] = expired_at.to_i
    JWT.encode(payload, Rails.application.secrets.secret_key_base)
  rescue StandardError
    nil
  end

  def self.decode(token)
    payload = JWT.decode(token, Rails.application.secrets.secret_key_base)[0]
    HashWithIndifferentAccess.new(payload)
  rescue StandardError
    nil
  end
end
