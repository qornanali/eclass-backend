module GuidGenerator
  def self.included(base)
    base.instance_eval do
      after_create do
        generated_guid = "#{self.class.name.upcase}-#{id}-#{created_at.to_i.to_s(36)}"
        update_column(:guid, generated_guid) unless guid.present?
      end
    end
  end
end
