module Cms
  class SubjectSerializer < ActiveModel::Serializer
    attributes :guid, :name

    attribute :created_at do
      object.created_at.to_datetime.rfc3339
    end

    attribute :updated_at do
      object.updated_at.to_datetime.rfc3339
    end
  end
end
