module Web
  class ScheduleSerializer < ActiveModel::Serializer
    attributes :day, :guid, :time, :duration
  end
end
