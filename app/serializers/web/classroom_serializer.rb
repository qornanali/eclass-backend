module Web
  class ClassroomSerializer < ActiveModel::Serializer
    attributes :name, :guid, :description
  end
end
