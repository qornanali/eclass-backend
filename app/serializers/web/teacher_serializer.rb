module Web
  class TeacherSerializer < ActiveModel::Serializer
    attributes :full_name, :guid, :bio, :gender

    attribute :email do
      object.account.email
    end
  end
end
