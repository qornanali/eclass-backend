module Web
  class UserSerializer < ActiveModel::Serializer
    attributes :email
  end
end
