# eclass backend

Monolith Backend Service for Eclass

Hosted at https://gitlab.com/qornanali/eclass-backend

## Getting Started

### Prequisites

1. JRuby v 9.2.0.0

JRuby is the java implementation of ruby, we use it to build the program.
You can install it using [Rbenv](https://github.com/rbenv/rbenv#installation)
or [RVM](https://rvm.io/rvm/install) and then follow the ruby installation that instructed there.

2. PostgreSQL 11.3

PostgreSQL is the DBMS we used to store and manage data.
You can install it by following the instructions in this [page](https://www.postgresql.org/download/).

3. Bundler 2.0.2

Bundle is [Rubygem](https://guides.rubygems.org/rubygems-basics/) that we used to manage this project's gems.
You can think gem as a library.

For installation, please execute this in your terminal:

```console
$ gem install bundle -v 2.0.2
```

### Setup the project

1. Set application config

Open this project directory on terminal and then execute this command below:

```console
$ cp config/application.yml.sample config/application.yml
```

**Modify the values**

`application.yml` file contains all keys and values that will be loaded when the app start. You can leave it as it is or put your own value.

2. Create database

You can create by your own or use script that provided by Rails.
Open this project directory on your terminal, and then execute:

```console
$ bin/rails db:create
```

**Loading latest schema**

```console
$ RAILS_ENV=test bin/rails db:migrate
$ RAILS_ENV=development bin/rails db:migrate
```

3. Load the dependencies

**Install Rubygems**

Open this project directory on terminal and then execute this command below:

```console
$ bundle install --path vendor/bundle
```

### Running the server locally

Open this project directory on terminal and then execute this command below:

```console
$ bin/rails s
```

It will defaultly run the server on `http://localhost:3000`.

## When developing

To maintain the code quality, these section below need to be checked everytime you want to open a merge request.

### Check specifications

To make sure your changes doesn't break anything, you can run
the unit tests (we call it spec).
Open this project directory on terminal and then execute this command below:

```console
$ bundle exec rspec
```

Please make sure your change does not decrease the code coverage.

### Check code smell and style

To make sure your changes follows the convention,
open this project directory on terminal and then execute this command below:

```console
$ bundle exec rubocop
$ bundle exec reek
```

## Documentation

### REST API

Postman collections: https://documenter.getpostman.com/view/7633499/SzKZsbtF

### Entity Relationship Diagram

![eclass-backend ERD](erd.png)

## Author

Ali Qornan

Bandung, Indonesia |
[Linkedin](https://www.linkedin.com/in/aliqornan) |
[Email](mailto:aliqornan97@gmail.com)
