Rails.application.routes.draw do
  get '_ping' => 'application#ping'

  scope module: :api, defaults: { format: :json } do
    scope path: :admin, module: :admin do
      scope path: :v1, module: :v1 do
        resources :cms_users, only: [:create]
      end
    end
  end

  scope module: :api, defaults: { format: :json } do
    scope path: :cms, module: :cms do
      scope path: :v1, module: :v1 do
        resources :subjects, param: :guid
      end
    end
  end

  scope module: :api, defaults: { format: :json } do
    scope path: :web, module: :web do
      scope path: :v1, module: :v1 do
        resource :user, only: [:create]
        resource :session, only: [:create]
        resources :classrooms, param: :guid, only: [:create, :index] do
          resources :schedules, only: [:index, :create], module: :classrooms
          resources :students, only: [:index, :create], module: :classrooms
          post 'leave', to: 'classrooms#leave'
          post 'enroll', to: 'classrooms#enroll'
          post 'apply', to: 'classrooms#apply'
          post 'resign', to: 'classrooms#resign'
          resources :teachers, only: [:index], module: :classrooms
        end
      end
    end
  end
end
