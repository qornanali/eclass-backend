source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.5.0', engine: 'jruby', engine_version: '9.2.0.0'

gem 'active_model_serializers'
gem 'activerecord-jdbcpostgresql-adapter'
gem 'bcrypt'
gem 'figaro', '~> 1.1'
gem 'jwt'
gem 'puma', '~> 4.1'
gem 'rails', '~> 6.0.1'
gem 'redis', '~> 4.0'
gem 'redis-rails', '~> 5.0'
gem 'sidekiq'

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'factory_bot_rails', '5.1.1'
  gem 'faker', '2.7.0'
  gem 'pry'
  gem 'reek', '5.5.0'
  gem 'rubocop', '0.76.0'
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'rails-erd'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  gem 'database_cleaner'
  gem 'json_matchers'
  gem 'rails-controller-testing'
  gem 'rspec-rails', '3.9.0'
  gem 'shoulda-matchers'
  gem 'simplecov', require: false
  gem 'simplecov-console', require: false
end

gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
