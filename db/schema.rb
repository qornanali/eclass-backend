# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_03_01_191051) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "attendances", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "study_session_id"
    t.bigint "web_user_id"
    t.index ["study_session_id"], name: "index_attendances_on_study_session_id"
    t.index ["web_user_id"], name: "index_attendances_on_web_user_id"
  end

  create_table "classrooms", force: :cascade do |t|
    t.string "name"
    t.string "guid"
    t.string "description"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["guid"], name: "index_classrooms_on_guid", unique: true
  end

  create_table "cms_users", force: :cascade do |t|
    t.string "name"
    t.string "client_id"
    t.string "password_digest"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["client_id"], name: "index_cms_users_on_client_id", unique: true
  end

  create_table "employments", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "teacher_id"
    t.bigint "classroom_id"
    t.index ["classroom_id"], name: "index_employments_on_classroom_id"
    t.index ["teacher_id"], name: "index_employments_on_teacher_id"
  end

  create_table "enrollments", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "student_id"
    t.bigint "classroom_id"
    t.index ["classroom_id"], name: "index_enrollments_on_classroom_id"
    t.index ["student_id"], name: "index_enrollments_on_student_id"
  end

  create_table "lectures", force: :cascade do |t|
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "subject_id"
    t.bigint "classroom_id"
    t.index ["classroom_id"], name: "index_lectures_on_classroom_id"
    t.index ["subject_id"], name: "index_lectures_on_subject_id"
  end

  create_table "schedules", force: :cascade do |t|
    t.string "guid"
    t.string "time"
    t.string "day"
    t.integer "duration"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "classroom_id"
    t.index ["classroom_id"], name: "index_schedules_on_classroom_id"
    t.index ["guid"], name: "index_schedules_on_guid", unique: true
  end

  create_table "students", force: :cascade do |t|
    t.string "full_name"
    t.string "guid"
    t.string "bio"
    t.string "gender"
    t.string "phone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "web_users_id"
    t.index ["guid"], name: "index_students_on_guid", unique: true
    t.index ["web_users_id"], name: "index_students_on_web_users_id"
  end

  create_table "study_sessions", force: :cascade do |t|
    t.string "guid"
    t.string "attendance_pass_key"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "schedule_id"
    t.index ["guid"], name: "index_study_sessions_on_guid", unique: true
    t.index ["schedule_id"], name: "index_study_sessions_on_schedule_id"
  end

  create_table "subjects", force: :cascade do |t|
    t.string "name"
    t.string "guid"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["guid"], name: "index_subjects_on_guid", unique: true
  end

  create_table "teachers", force: :cascade do |t|
    t.string "full_name"
    t.string "guid"
    t.string "bio"
    t.string "gender"
    t.string "phone"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "web_users_id"
    t.index ["guid"], name: "index_teachers_on_guid", unique: true
    t.index ["web_users_id"], name: "index_teachers_on_web_users_id"
  end

  create_table "web_user_sessions", force: :cascade do |t|
    t.string "auth_token"
    t.datetime "expired_at"
    t.bigint "web_users_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["web_users_id"], name: "index_web_user_sessions_on_web_users_id"
  end

  create_table "web_users", force: :cascade do |t|
    t.string "email"
    t.string "password_digest"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_web_users_on_email", unique: true
  end

  add_foreign_key "students", "web_users", column: "web_users_id"
  add_foreign_key "teachers", "web_users", column: "web_users_id"
  add_foreign_key "web_user_sessions", "web_users", column: "web_users_id"
end
