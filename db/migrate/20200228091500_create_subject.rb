class CreateSubject < ActiveRecord::Migration[6.0]
  def change
    create_table :subjects do |t|
      t.string :name
      t.string :guid

      t.timestamps

      t.index :guid, unique: true
    end
  end
end
