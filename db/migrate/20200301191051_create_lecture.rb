class CreateLecture < ActiveRecord::Migration[6.0]
  def change
    create_table :lectures do |t|
      t.timestamps

      t.belongs_to :subject
      t.belongs_to :classroom
    end
  end
end
