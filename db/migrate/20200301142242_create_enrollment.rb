class CreateEnrollment < ActiveRecord::Migration[6.0]
  def change
    create_table :enrollments do |t|
      t.timestamps

      t.belongs_to :student
      t.belongs_to :classroom
    end
  end
end
