class CreateStudySession < ActiveRecord::Migration[6.0]
  def change
    create_table :study_sessions do |t|
      t.string :guid
      t.string :attendance_pass_key
      t.timestamps

      t.index :guid, unique: true

      t.belongs_to :schedule
    end
  end
end
