class CreateCmsUser < ActiveRecord::Migration[6.0]
  def change
    create_table :cms_users do |t|
      t.string :name
      t.string :client_id
      t.string :password

      t.timestamps

      t.index :client_id, unique: true
    end
  end
end
