class CreateWebUserSession < ActiveRecord::Migration[6.0]
  def change
    create_table :web_user_sessions do |t|
      t.string :auth_token
      t.timestamp :expired_at
      t.references :web_users, foreign_key: true

      t.timestamps
    end
  end
end
