class CreateEmployment < ActiveRecord::Migration[6.0]
  def change
    create_table :employments do |t|
      t.timestamps

      t.belongs_to :teacher
      t.belongs_to :classroom
    end
  end
end
