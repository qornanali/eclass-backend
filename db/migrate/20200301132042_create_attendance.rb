class CreateAttendance < ActiveRecord::Migration[6.0]
  def change
    create_table :attendances do |t|
      t.timestamps

      t.belongs_to :study_session
      t.belongs_to :web_user
    end
  end
end
