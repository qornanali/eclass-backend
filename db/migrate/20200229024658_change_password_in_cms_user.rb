class ChangePasswordInCmsUser < ActiveRecord::Migration[6.0]
  def self.up
    rename_column :cms_users, :password, :password_digest
  end

  def self.down
    rename_column :cms_users, :password_digest, :password
  end
end
