class CreateClassroom < ActiveRecord::Migration[6.0]
  def change
    create_table :classrooms do |t|
      t.string :name
      t.string :guid
      t.string :description
      t.timestamps

      t.index :guid, unique: true
    end
  end
end
