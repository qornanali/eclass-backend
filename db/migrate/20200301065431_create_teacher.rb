class CreateTeacher < ActiveRecord::Migration[6.0]
  def change
    create_table :teachers do |t|
      t.string :full_name
      t.string :guid
      t.string :bio
      t.string :gender
      t.string :phone

      t.timestamps

      t.index :guid, unique: true
      t.references :web_users, foreign_key: true
    end
  end
end
