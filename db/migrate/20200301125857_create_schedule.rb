class CreateSchedule < ActiveRecord::Migration[6.0]
  def change
    create_table :schedules do |t|
      t.string :guid
      t.string :time
      t.string :day
      t.integer :duration
      t.timestamps

      t.index :guid, unique: true

      t.belongs_to :classroom
    end
  end
end
